# README

This Application will continuosly (100 times in every 5 minutes) search for vaccine slots in your preferred districts.  
Once it has slots available in your district it will automatically book that particular slot,  
although it will ask you to enter the OTP which you'll receive on your registered mobile number.

`PS: only use your registered mobile number with which you have registered on the co-win website`


Things you may want to cover after you have cloned the repo and entered into the repo directory:

* Ruby version
  2.7.3
* System Dependencies
  1. Mysql
  2. Api key of Anti Captcha from [Anti Captcha](https://anti-captcha.com/mainpage)
      - Sign up, then goto API settings
      - Generate Key and copy
      - Also goto funds add fund and add free test balance to be able to use the service for free
  3. Api key of Cloud Convert from [Cloud Convert](https://www.cloundconvert.com) (V1 Api Key)
      - Sign up, then goto Authencation V1
      - Generate Key and copy

* Setup Instructions (MAC with brew preinstalled)

* Installing Ruby
  1. Install rvm(ruby version manager)

    ``` shell
      brew install gnupg
      gpg --keyserver hkp://pool.sks-keyservers.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
      \curl -sSL https://get.rvm.io | bash
      rvm install 2.7.3
    ```

  2. Project related setup

    ``` shell
      bundle install
      rails db:setup
      rails db:migrate
    ```
  
  3. Running setup
    GOTO lib/tasks/setup.rake
    run that onto your rails console

     OR

    ``` ruby
      Cowin::States.call
      State.all.each do |state|
        Cowin::Districts.call(state.id)
      end
    ```

  4. Call StartBooking with all the parameters to start booking

    ``` ruby
      StartBooking.call({districts: ['your_district_id/ids'], beneficiaries: [reference_id], mobile_number: "your_mobile_number"})
    ```
    
    `
      reference_id: You can find this on the cowin website from [Cowin](https://www.cowin.gov.in/home)  
      your_district_id/ids: You can query(given below) this on you system after Step 3
    `
    

    ``` ruby
      State.find_by(state_name: "Your State Name").districts
    ```
    `Get all the district_id/s you are looking for and place it onto above start booking call`
  
* ...