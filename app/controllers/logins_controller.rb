# frozen_string_literal: true

class LoginsController < ApplicationController
  layout 'application'

  def login; end
end
