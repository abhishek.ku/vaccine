# frozen_string_literal: true

class District < ApplicationRecord
  belongs_to :state, primary_key: 'state_id', foreign_key: 'state_id'
end
