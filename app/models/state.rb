# frozen_string_literal: true

class State < ApplicationRecord
  has_many :districts, primary_key: 'state_id', foreign_key: 'state_id'
end
