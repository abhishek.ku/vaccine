# frozen_string_literal: true

require 'dry/monads/all'

class ApplicationService #:nodoc:
  extend Dry::Initializer
  include Dry::Monads

  def self.call(*args)
    new(*args).call
  end

  def call
    raise NotImplementedError, 'You must #call method'
  end
end
