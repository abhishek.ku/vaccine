# frozen_string_literal: true

class Authenticate < ApplicationService #:nodoc:
  API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
  OTP_GEN_API_URL = '/v2/auth/generateMobileOTP'
  OTP_VAL_API_URL = '/v2/auth/validateMobileOtp'
  API_METHOD = 'POST'

  param :mobile_number
  option :secret, default: proc { '' }
  option :gen_endpoint, default: proc { API_ENDPOINT + OTP_GEN_API_URL }
  option :val_endpoint, default: proc { API_ENDPOINT + OTP_VAL_API_URL }

  def call
    send_otp
  end

  private

  def send_otp
    result = RestClient.call(API_METHOD, @gen_endpoint, payload: generate_payload)
    if result.success?
      system("osascript -e 'display notification \"OTP sent\" sound name \"Glass\" with title \"Vaccine Booked\" '")
      validate(result.success[:body]['txnId'])
    else
      result
    end
  end

  def validate(transation_id)
    result = RestClient.call(API_METHOD, @val_endpoint, payload: val_payload(transation_id), override_headers: headers)
    if result.success?
      system("osascript -e 'display notification \"OTP validated\" sound name \"Glass\" with title \"Vaccine Booked\" '")
      Success(result.success[:body])
    else
      result
    end
  end

  def headers
    {
      authority: 'cdn-api.co-vin.in',
      origin: 'https://selfregistration.cowin.gov.in',
      referer: 'https://selfregistration.cowin.gov.in/'
    }
  end

  def val_payload(transation_id)
    Rails.logger.info("Enter the OTP received on: #{@mobile_number}")
    {
      'otp': Digest::SHA256.hexdigest(gets.chomp.to_s),
      'txnId': transation_id
    }
  end

  def generate_payload
    {
      secret: 'U2FsdGVkX19mSeLk25aVTbDOXvzI2H7EWtt7GmP40GUWzI/JX7yGqP6i6IvmXi16lJghPhUomCj1FMUtUzumMg==',
      mobile: @mobile_number
    }
  end
end
