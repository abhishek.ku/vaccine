# frozen_string_literal: true

require 'uri'
require 'json'
require 'net/http'

module Captcha
  class Convert < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://api.cloudconvert.com/v1/convert'
    API_METHOD = 'POST'

    param :svg_string
    option :endpoint, default: proc { API_ENDPOINT }

    def call
      book
    end

    private

    def book
      url = URI(@endpoint)
      https = Net::HTTP.new(url.host, url.port)
      https.use_ssl = true
      request = Net::HTTP::Post.new(url)
      request['Content-Type'] = 'application/json'
      request.body = JSON.dump(payload)
      response = https.request(request)

      if response.code.eql?('303')
        Success(data: response.read_body.remove('See Other. Redirecting to '))
      else
        Failure(data: response)
      end
    end

    def payload
      {
        inputformat: 'svg',
        outputformat: 'jpg',
        input: 'raw',
        wait: true,
        download: 'true',
        filename: 'praneet.svg',
        apikey: 'cd4vUj4IiwWNAR1XdZsMWiJ6I4826E9OrgJ33WWxfCuY48zc39ytgFW5FH6tsKFj',
        file: @svg_string
      }
    end
  end
end
