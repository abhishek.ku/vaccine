# frozen_string_literal: true

require 'open-uri'

module Captcha
  class Engage < ApplicationService #:nodoc:
    param :auth

    def call
      get_captcha
    end

    private

    def get_captcha
      result = Cowin::GetCaptcha.call(@auth)
      if result.success?
        convert_captcha(result.success[:data])
      else
        Failure(result.failure)
      end
    end

    def convert_captcha(svg_string)
      result = Captcha::Convert.call(svg_string)
      if result.success?
        file = open(result.success[:data])
        Success(data: Captcha::Text.call(file.read))
      else
        Failure(result.failure)
      end
    end
  end
end
