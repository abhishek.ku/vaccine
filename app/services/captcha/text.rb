# frozen_string_literal: true

module Captcha
  class Text < ApplicationService #:nodoc:
    param :file

    def call
      text
    end

    private

    def text
      solution = CAPTCHA_CLIENT.decode_image!(body: @file)
      solution.text
    end
  end
end
