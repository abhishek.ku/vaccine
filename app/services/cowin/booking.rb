# frozen_string_literal: true

module Cowin
  class Booking < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
    API_URL = '/v2/appointment/schedule'
    API_METHOD = 'POST'

    param :session_id
    param :slot
    param :center_id
    param :captcha
    option :beneficiaries, default: proc { ['91568339770260'] }
    option :dose, default: proc { 1 }
    option :auth, default: proc { '' }
    option :endpoint, default: proc { API_ENDPOINT + API_URL }

    def call
      book
    end

    private

    def book
      result = RestClient.call(API_METHOD, @endpoint, payload: payload, override_headers: headers, log: true)
      if result.success? && result.success[:status_code].eql?(200)
        system("osascript -e 'display notification \"Booking Done\" sound name \"Glass\" with title \"Vaccine Booked\" '")
        Success(data: result.success[:data])
      else
        result
      end
    end

    def payload
      {
        "dose": @dose,
        "center_id": @center_id,
        "captcha": @captcha,
        "session_id": @session_id,
        "slot": @slot,
        "beneficiaries": @beneficiaries
      }
    end

    def headers
      {
        authorization: @auth,
        authority: 'cdn-api.co-vin.in',
        origin: 'https://selfregistration.cowin.gov.in',
        referer: 'https://selfregistration.cowin.gov.in/'
      }
    end
  end
end
