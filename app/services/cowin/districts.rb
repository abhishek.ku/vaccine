# frozen_string_literal: true

module Cowin
  class Districts < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
    API_URL = '/v2/admin/location/districts/'
    API_METHOD = 'GET'

    param :state, (proc { |value| State.find(value) })
    option :endpoint, default: proc { API_ENDPOINT + API_URL + @state.state_id.to_s }

    def call
      fetch
    end

    private

    def fetch
      result = RestClient.call(API_METHOD, @endpoint)
      if result.success?
        data = result.success.dig(:body, 'districts')
        (data || []).each do |district|
          ::Districts::Create.call(district['district_id'], district.merge(state_id: @state.state_id))
        end
      else
        result
      end
    end
  end
end
