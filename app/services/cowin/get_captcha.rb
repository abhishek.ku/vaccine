# frozen_string_literal: true

module Cowin
  class GetCaptcha < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
    API_URL = '/v2/auth/getRecaptcha'
    API_METHOD = 'POST'

    param :auth
    option :gen_endpoint, default: proc { API_ENDPOINT + API_URL }

    def call
      generate
    end

    private

    def generate
      result = RestClient.call(API_METHOD, @gen_endpoint, override_headers: headers)
      if result.success?
        Success(data: result.success[:body]['captcha'])
      else
        result
      end
    end

    def headers
      {
        authority: 'cdn-api.co-vin.in',
        origin: 'https://selfregistration.cowin.gov.in',
        referer: 'https://selfregistration.cowin.gov.in/',
        authorization: "Bearer #{auth}"
      }
    end
  end
end
