# frozen_string_literal: true

module Cowin
  class SessionsDistricts < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
    API_URL = '/v2/appointment/sessions/public/findByDistrict?'
    API_METHOD = 'GET'

    param :district, (proc { |value| District.find_by(district_id: value) })
    option :date, default: proc { Date.today.strftime('%d-%m-%Y') }
    option :endpoint, default: proc { API_ENDPOINT + API_URL + "district_id=#{@district.district_id}&date=#{@date}" }

    def call
      availability
    end

    def availability
      result = RestClient.call(API_METHOD, @endpoint)
      if result.success? && !result.success.dig(:body, 'sessions').blank?
        Success(data: result.success.dig(:body, 'sessions'))
      else
        Failure(message: 'No slots found')
      end
    end
  end
end
