# frozen_string_literal: true

module Cowin
  class States < ApplicationService #:nodoc:
    API_ENDPOINT = 'https://cdn-api.co-vin.in/api'
    API_URL = '/v2/admin/location/states'
    API_METHOD = 'GET'

    option :endpoint, default: proc { API_ENDPOINT + API_URL }

    def call
      fetch
    end

    private

    def fetch
      result = RestClient.call(API_METHOD, @endpoint)
      if result.success?
        data = result.success.dig(:body, 'states')
        (data || []).each do |state|
          ::States::Create.call(state['state_id'], state)
        end
      else
        result
      end
    end
  end
end
