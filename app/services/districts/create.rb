# frozen_string_literal: true

module Districts
  class Create < ApplicationService #:nodoc:
    param :district, proc { |value| District.find_or_initialize_by(district_id: value) }
    param :create_params

    def call
      create
    end

    private

    def create
      @district.district_name = create_params['district_name']
      @district.state_id = create_params[:state_id]
      if @district.save
        Success(message: 'district Created', data: @district)
      else
        Failure(message: @district.errors.full_messages.to_sentence)
      end
    end
  end
end
