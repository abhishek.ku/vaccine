# frozen_string_literal:  true

require 'http'

class RestClient < ApplicationService #:nodoc:
  METHODS = %w[GET POST PUT PATCH DELETE].freeze
  NO_METHOD_ERROR = 'Method not included in the list'
  ACCEPTED_HEADERS = %w[access-token uid client].freeze

  param :method, (proc { |value| value.upcase })
  param :endpoint, (proc { |value| URI(value) })
  option :payload, default: proc { {} }
  option :query_payload, default: proc { {} }
  option :override_headers, default: proc { {} }

  # These options implies that this option should not be passed,
  # while calling this interactor
  option :content_type, default: proc { 'application/json' }
  option :accept, default: proc { 'application/json' }
  option :headers, default: proc { get_headers }
  option :parse, default: proc { true }
  option :log, default: proc { false }

  def call
    @start_time = Time.now
    request
    if @code.to_i >= 200 && @code <= 299
      Success(status_code: @code, body: @body)
    else
      Failure(status_code: @code, body: @body)
    end
  rescue StandardError => e
    Rails.logger.info("REQUEST FAILED: #{e.message} Time taken:- #{Time.now - @start_time} sec")
    Failure(message: e.message)
  end

  private

  def request
    @response = send(@method.downcase)
    parse_response
    Rails.logger.info("Finised with response: #{@response.status.inspect}") if @log
  end

  def get
    HTTP.headers(@headers).get(@endpoint)
  end

  def post
    HTTP.headers(@headers).post(@endpoint, json: @payload)
  end

  def put
    HTTP.headers(@headers).put(@endpoint, json: @payload)
  end

  def patch
    HTTP.headers(@headers).patch(@endpoint, json: @payload)
  end

  def delete
    HTTP.headers(@headers).delete(@endpoint)
  end

  def get_headers
    {
      'X-Request-Id': SecureRandom.hex(16),
      'Content-Type': @content_type,
      'accept': @accept
    }.merge(@override_headers)
  end

  def parse_response
    @code = @response.code
    @body = parse ? JSON.parse(@response.body) : @response.body
  end
end
