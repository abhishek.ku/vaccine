# frozen_string_literal: true

# StartBooking.call(mobile_number: 9535226415, beneficiaries: [25225245697010, 20313372531220], districts: [730], age: 45)
# call this Service to start the bot

class StartBooking < ApplicationService #:nodoc:
  option :districts, default: proc { [247] }
  option :age, default: proc { 18 }
  option :final, default: proc { [] }
  option :beneficiaries, default: proc { ['51104409948200'] }
  option :mobile_number, default: proc { '6203313084' }
  option :failure, default: proc { [] }

  def call
    Rails.logger.info('Searching for vaccine slots, please wait and do not press enter!')
    while @final.blank?
      check
      sleep((@districts.count.to_f * 3.3).ceil)
    end
    [book, @failure]
  end

  private

  def book
    Rails.logger.info('Slots found, Commencing booking')
    result = Authenticate.call(@mobile_number)
    if result.success?
      @final.each do |hospital|
        Rails.logger.info('Starting to solve captcha')
        captcha = Captcha::Engage.call(result.success['token'])
        next unless captcha.success?

        Rails.logger.info("Booking Slot: #{hospital[:name]}, count_left: #{hospital[:count]}, date: #{hospital[:date]}")
        booking = Cowin::Booking.call(hospital[:session_id],
                                      hospital[:slot],
                                      hospital[:center_id],
                                      captcha.success[:data],
                                      { beneficiaries: @beneficiaries, auth: "Bearer #{result.success['token']}" })
        if booking.success? && booking.success[:status_code].eql?(200)
          return booking
        else
          @failure << booking
        end
      end
    else
      result
    end
  end

  def check
    @result = []
    @districts.each do |district_id|
      result = Cowin::SessionsDistricts.call(district_id)
      next unless result.success?

      @result += result.success[:data]
    end
    validate
  end

  def validate
    @result.each do |hash|
      next unless hash['available_capacity_dose1'] > 1
      next unless hash['min_age_limit'].eql?(@age)

      @final << { district: hash['district_name'],
                  date: hash['date'],
                  name: hash['name'],
                  count: hash['available_capacity_dose1'],
                  session_id: hash['session_id'],
                  slot: hash['slots'].first,
                  center_id: hash['center_id'] }
    end
  end
end
