# frozen_string_literal: true

module States
  class Create < ApplicationService #:nodoc:
    param :state, proc { |value| State.find_or_initialize_by(state_id: value) }
    param :create_params

    def call
      create
    end

    private

    def create
      @state.state_name = create_params['state_name']
      if @state.save
        Success(message: 'State Created', data: @state)
      else
        Failure(message: @state.errors.full_messages.to_sentence)
      end
    end
  end
end
