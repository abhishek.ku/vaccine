# frozen_string_literal: true

class CreateStates < ActiveRecord::Migration[6.1]
  def change
    create_table :states do |t|
      t.integer :state_id
      t.string :state_name
      t.string :state_name_1

      t.timestamps
    end
  end
end
