# frozen_string_literal: true

class CreateDistricts < ActiveRecord::Migration[6.1]
  def change
    create_table :districts do |t|
      t.string :district_name
      t.integer :district_id
      t.integer :state_id

      t.timestamps
    end
  end
end
