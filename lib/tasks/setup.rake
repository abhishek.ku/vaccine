# frozen_string_literal: true

namespace :setup do
  desc 'Setup cowin vaccine bot'
  task :cowin do
    Cowin::States.call
    State.all.each do |state|
      Cowin::Districts.call(state.id)
    end
  end
end
